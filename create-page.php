<?php
	include_once 'config.php';				// Import config.php File
	include_once 'helper/dbconnect.php';	// Returns $db_connect which represents the connection to a MySQL Server.

	// Perform database query
	$result = mysqli_query($db_connect,"SELECT * FROM datatable where todo='1' LIMIT 1");
	if (!$result) {
		die("Database query failed: " . mysqli_error());
	}

	// Use returned data
	$row = mysqli_fetch_array($result);

	include $layoutsdir.'layout.php';

	mysqli_query($db_connect,"update datatable set todo='0' where id=".$row["id"]);
	
	// Close connection
	mysqli_close($db_connect);
?>
