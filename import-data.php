<?php
	include_once 'config.php';									// Import config.php File
	include_once 'helper/dbconnect.php';						// Returns $db_connect which represents the connection to a MySQL Server.
	include_once 'helper/string_functions.php';						// Include String manipulation functions
	
?>
<!doctype html>
<html>
<head>
<script language="JavaScript" type="text/javascript"><!--
	setTimeout("window.history.go(-1)",<?php echo $resettime_slow?>);
--></script>
<meta charset="utf-8">
<title>Import HTML-Snippets to DB</title>
</head>
<body>
<?php
	# Check if Input Directory exist
	if (!file_exists($imputdir)) {
	    mkdir($imputdir, 0777, true);
		echo "Verzeichins ".$imputdir." war nicht vorhanden... <br />Verzeichins ".$imputdir." angelegt";
	}
	else{
		# Import TEXT
		foreach ($htmlTextBlocks as $htmlTextBlock) {
			$htmlTextBlock["filecontent"] = "";
			for ($i = 1; $i <= $num_newsletters; $i++) {
				if (file_exists($imputdir."/".$htmlTextBlock['name']."-".sprintf( '%02d', $i ).".html")) {
					$htmlTextBlock['filecontent'] = file_get_contents($imputdir."/".$htmlTextBlock['name']."-".sprintf( '%02d', $i ).".html");
					$htmlTextBlock['filecontent'] = NormText($htmlTextBlock['filecontent'],$htmlTextBlock['num_tabs']);
					$result = mysqli_query($db_connect,"update datatable set ".$htmlTextBlock['name']."='".$htmlTextBlock['filecontent']."' where id=".$i);
					if ($result == 1){
						echo "Datei ".$imputdir."/".$htmlTextBlock['name']."-".sprintf( '%02d', $i ).".html importiert...<br />";
					}
					else{
						echo "SQL fehler bei Datei ".$imputdir."/".$htmlTextBlock['name']."-".sprintf( '%02d', $i ).".html...<br />";
						echo "failed. SQL Err: ". mysql_error() ."<br />";
					}
				}
				else{
					echo "<span style=\"color:#991111\">Datei ".$imputdir."/".$htmlTextBlock['name']."-".sprintf( '%02d', $i ).".html existiert nicht.</span><br />";
				}
			}	
		}

		# Import Strings
		foreach ($htmlStrings as $htmlString) {
			$htmlStringList  = array ("filecontent" => "");
			if (file_exists($imputdir."/".$htmlString.".txt")) {
				$htmlStringList["filecontent"] = file($imputdir."/".$htmlString.".txt");
				for ($i = 1; $i <= $num_newsletters; $i++) {
					$result = mysqli_query($db_connect,"update datatable set ".$htmlString."='".NormString($htmlStringList["filecontent"][$i-1])."' where id=".$i);
					if ($result == 1){
						echo "Zeile ".sprintf( '%02d', $i )." aus Datei ".$imputdir."/".$htmlString.".txt importiert...<br />";
					}
					else{
						echo "SQL fehler bei Datei ".$imputdir."/".$htmlString.".txt Zeile ".sprintf( '%02d', $i )."...<br />";
						echo "failed. SQL Err: ". mysql_error() ."<br />";
					}
				}
			}
			else{
				echo "<span style=\"color:#991111\">Datei ".$imputdir."/".$htmlTextBlock['name']."-".sprintf( '%02d', $i ).".html existiert nicht.</span><br />";
			}				
		}		
	}
	// Close DB connection
	mysqli_close($db_connect);
?>
</body>
</html>
