<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Email Campaign Generator v0.2</title>
<style type="text/css">
body{text-align:center;}
p {margin-bottom:2em;}
.btn {
	-webkit-border-top-left-radius:4px;
	-moz-border-radius-topleft:4px;
	border-top-left-radius:4px;
	-webkit-border-top-right-radius:4px;
	-moz-border-radius-topright:4px;
	border-top-right-radius:4px;
	-webkit-border-bottom-right-radius:4px;
	-moz-border-radius-bottomright:4px;
	border-bottom-right-radius:4px;
	-webkit-border-bottom-left-radius:4px;
	-moz-border-radius-bottomleft:4px;
	border-bottom-left-radius:4px;
	text-indent:0px;
	display:inline-block;
	font-family:Arial;
	font-size:16px;
	font-weight:bold;
	font-style:normal;
	height:40px;
	line-height:40px;
	width:220px;
	text-decoration:none;
	text-align:center;

}
.btn:active {
	position:relative;
	top:1px;
}

.btn.output {
	-moz-box-shadow:inset 0px 1px 0px 0px #bee2f9;
	-webkit-box-shadow:inset 0px 1px 0px 0px #bee2f9;
	box-shadow:inset 0px 1px 0px 0px #bee2f9;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #63b8ee), color-stop(1, #468ccf) );
	background:-moz-linear-gradient( center top, #63b8ee 5%, #468ccf 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#63b8ee', endColorstr='#468ccf');
	background-color:#63b8ee;
	border:1px solid #3866a3;
	color:#14396a;
	text-shadow:1px 1px 0px #7cacde;
}
.btn.output:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #468ccf), color-stop(1, #63b8ee) );
	background:-moz-linear-gradient( center top, #468ccf 5%, #63b8ee 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#468ccf', endColorstr='#63b8ee');
	background-color:#468ccf;
}

.btn.start {
	-moz-box-shadow:inset 0px 1px 0px 0px #caefab;
	-webkit-box-shadow:inset 0px 1px 0px 0px #caefab;
	box-shadow:inset 0px 1px 0px 0px #caefab;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #77d42a), color-stop(1, #5cb811) );
	background:-moz-linear-gradient( center top, #77d42a 5%, #5cb811 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#77d42a', endColorstr='#5cb811');
	background-color:#77d42a;
	border:1px solid #268a16;
	color:#306108;
	text-shadow:1px 1px 0px #aade7c;
}
.btn.start:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #5cb811), color-stop(1, #77d42a) );
	background:-moz-linear-gradient( center top, #5cb811 5%, #77d42a 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#5cb811', endColorstr='#77d42a');
	background-color:#5cb811;
}

.btn.reset {
	-moz-box-shadow:inset 0px 1px 0px 0px #f5978e;
	-webkit-box-shadow:inset 0px 1px 0px 0px #f5978e;
	box-shadow:inset 0px 1px 0px 0px #f5978e;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #f24537), color-stop(1, #c62d1f) );
	background:-moz-linear-gradient( center top, #f24537 5%, #c62d1f 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f24537', endColorstr='#c62d1f');
	background-color:#f24537;
	border:1px solid #d02718;
	color:#ffffff;
	text-shadow:1px 1px 0px #810e05;
}
.btn.reset:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #c62d1f), color-stop(1, #f24537) );
	background:-moz-linear-gradient( center top, #c62d1f 5%, #f24537 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#c62d1f', endColorstr='#f24537');
	background-color:#c62d1f;
}

.btn.import {
	-moz-box-shadow:inset 0px 1px 0px 0px #d197fe;
	-webkit-box-shadow:inset 0px 1px 0px 0px #d197fe;
	box-shadow:inset 0px 1px 0px 0px #d197fe;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #a53df6), color-stop(1, #7c16cb) );
	background:-moz-linear-gradient( center top, #a53df6 5%, #7c16cb 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#a53df6', endColorstr='#7c16cb');
	background-color:#a53df6;
	border:1px solid #9c33ed;
	color:#ffffff;
	text-shadow:1px 1px 0px #7d15cd;
}
.btn.import:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #7c16cb), color-stop(1, #a53df6) );
	background:-moz-linear-gradient( center top, #7c16cb 5%, #a53df6 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#7c16cb', endColorstr='#a53df6');
	background-color:#7c16cb;
}

.btn.dbcreate {
	-moz-box-shadow:inset 0px 1px 0px 0px #f9eca0;
	-webkit-box-shadow:inset 0px 1px 0px 0px #f9eca0;
	box-shadow:inset 0px 1px 0px 0px #f9eca0;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #f0c911), color-stop(1, #f2ab1e) );
	background:-moz-linear-gradient( center top, #f0c911 5%, #f2ab1e 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f0c911', endColorstr='#f2ab1e');
	background-color:#f0c911;
	border:1px solid #e6ad45;
	color:#c92200;
	text-shadow:1px 1px 0px #ded17c;
}
.btn.dbcreate:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #f2ab1e), color-stop(1, #f0c911) );
	background:-moz-linear-gradient( center top, #f2ab1e 5%, #f0c911 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f2ab1e', endColorstr='#f0c911');
	background-color:#f2ab1e;
}
</style>
</head>

<body>
<h3>Email Campaign Generator v0.2</h3>
<p>
<a href="create-page.php" class="btn output">Goto: Output Seite</a>
</p>
<p>
<a href="start-creating.php" class="btn start">Start-Creating</a>
</p>
<p>
<a href="import-data.php" class="btn import">Import Data</a>
</p>
<p>
<a href="reset.php" class="btn reset">Reset - Todo</a>
</p>
<p>
<a href="dbceate.php" class="btn dbcreate">Create new DB!</a>
</p>
</body>
</html>
