<?php
	// Create a database connection
	//		$db_connect allows us to keep refering to this connection after it is established
	$db_connect = mysqli_connect($host,$user,$pw);
	if (!$db_connect) {
		die("Database connection failed: " . mysqli_error());
	}

	// Select a database to use 
	$db_select = mysqli_select_db($db_connect,$db);
	if (!$db_select) {
		die("Database selection failed: " . mysqli_error());
	}
?>
