<?php
	define('CR', "/\x0d/");										// Carriage Return: Mac
	define('LF', "/\x0a/");										// Line Feed: Unix
	define('CRLF', "/\x0d\x0a/");								// Carriage Return and Line Feed: Windows
	define ('BOM', "/\xef\xbb\xbf/");							// UTF-8 BOM (Byte Order Mark)
	
	$encodings = array(
		'amp' => array("HTML" => "&amp;", "CP1256" => "/\x26/", "UTF-8" => "/\x26/u"),
		'euro' => array("HTML" => "&euro;", "CP1256" => "/\x80/", "UTF-8" => "/\xe2\x82\xac/u"),
		'bdquo' => array("HTML" => "&bdquo;", "CP1256" => "/\x84/", "UTF-8" => "/\xe2\x80\x9e/u"),
		'hellip' => array("HTML" => "&hellip;", "CP1256" => "/\x85/", "UTF-8" => "/\xe2\x80\xa6/u"),
		'permil' => array("HTML" => "&permil;", "CP1256" => "/\x89/", "UTF-8" => "/\xe2\x80\xb0/u"),
		'lsquo' => array("HTML" => "&lsquo;", "CP1256" => "/\x91/", "UTF-8" => "/\xe2\x80\x98/u"),
		'rsquo' => array("HTML" => "&rsquo;", "CP1256" => "/\x92/", "UTF-8" => "/\xe2\x80\x99/u"),
		'ldquo' => array("HTML" => "&ldquo;", "CP1256" => "/\x93/", "UTF-8" => "/\xe2\x80\x9c/u"),
		'rdquo' => array("HTML" => "&rdquo;", "CP1256" => "/\x94/", "UTF-8" => "/\xe2\x80\x9d/u"),
		'ndash' => array("HTML" => "&ndash;", "CP1256" => "/\x96/", "UTF-8" => "/\xe2\x80\x93/u"),
		'mdash' => array("HTML" => "&mdash;", "CP1256" => "/\x97/", "UTF-8" => "/\xe2\x80\x94/u"),
		'sect' => array("HTML" => "&sect;", "CP1256" => "/\xa7/", "UTF-8" => "/\xc2\xa7/u"),
		'copy' => array("HTML" => "&copy;", "CP1256" => "/\xa9/", "UTF-8" => "/\xc2\xa9/u"),
		'deg' => array("HTML" => "&deg;", "CP1256" => "/\xb0/", "UTF-8" => "/\xc2\xb0/u"),
		'sup2' => array("HTML" => "&sup2;", "CP1256" => "/\xb2/", "UTF-8" => "/\xc2\xb2/u"),
		'sup3' => array("HTML" => "&sup3;", "CP1256" => "/\xb3/", "UTF-8" => "/\xc2\xb3/u"),
		'acute' => array("HTML" => "&acute;", "CP1256" => "/\xb4/", "UTF-8" => "/\xc2\xb4/u"),
		'sup1' => array("HTML" => "&sup1;", "CP1256" => "/\xb9/", "UTF-8" => "/\xc2\xb9/u"),
		'Auml' => array("HTML" => "&Auml;", "CP1256" => "/\xc4/", "UTF-8" => "/\xc3\x84/u"),
		'Ouml' => array("HTML" => "&Ouml;", "CP1256" => "/\xd6/", "UTF-8" => "/\xc3\x96/u"),
		'Uuml' => array("HTML" => "&Uuml;", "CP1256" => "/\xdc/", "UTF-8" => "/\xc3\x9c/u"),
		'szlig' => array("HTML" => "&szlig;", "CP1256" => "/\xdf/", "UTF-8" => "/\xc3\x9f/u"),
		'auml' => array("HTML" => "&auml;", "CP1256" => "/\xe4/", "UTF-8" => "/\xc3\xa4/u"),
		'ouml' => array("HTML" => "&ouml;", "CP1256" => "/\xf6/", "UTF-8" => "/\xc3\xb6/u"),
		'uuml' => array("HTML" => "&uuml;", "CP1256" => "/\xfc/", "UTF-8" => "/\xc3\xbc/u")
	);
	
	mb_detect_order("ASCII,UTF-8,ISO-8859-1");
	
	function html_content($s) {
		global $encodings;
		$stingencoding = mb_detect_encoding($s);
		if ($stingencoding != "ASCII"){
			foreach ($encodings as $encoding) {
				if ($stingencoding == "UTF-8"){
					$s = preg_replace($encoding["UTF-8"],$encoding["HTML"],$s);
				}else {
					$s = preg_replace($encoding["CP1256"],$encoding["HTML"],$s);
				}
			}	
		}	
		return $s;
	}
		
	function NormText($s,$t) {
		global $db_connect;
		$s = preg_replace('/^/m', str_repeat("\t", $t), $s);	// Add leading Tabs
		$s = preg_replace(CRLF, "\n", $s);						// Replace Windows "End of Line" characters
		$s = preg_replace(CR, "\n", $s);						// Replace MAC OS "End of Line" characters
		$s = preg_replace(LF, "\n", $s);						// Replace UNIX "End of Line" characters
		$s = preg_replace("/\n{2,}/", "\n", $s);				// Remove more than 2 "End of Line" characters next to one another
		$s = preg_replace(BOM, "", $s);							// Remove UTF-8 BOM (Byte Order Mark)
		$s = html_content($s);									// Convert all applicable characters to HTML entities
		$s = mysqli_real_escape_string($db_connect,$s);			// Escapes special characters in a string for use in an SQL statement
		return $s;
	}
	
	function NormString($s) {
		global $db_connect;
		$s = preg_replace(CRLF, "", $s);						// Remove Windows "End of Line" characters
		$s = preg_replace(CR, "", $s);							// Remove MAC OS "End of Line" characters
		$s = preg_replace(LF, "", $s);							// Remove UNIX "End of Line" characters
		$s = preg_replace(BOM, "", $s);							// Remove UTF-8 BOM (Byte Order Mark)
		$s = html_content($s);									// Convert all applicable characters to HTML entities
		$s = mysqli_real_escape_string($db_connect,$s);			// Escapes special characters in a string for use in an SQL statement
		return $s;
	}
?>
