<?php
	include_once 'config.php';				// Import config.php File
	include_once 'helper/dbconnect.php';	// Returns $db_connect which represents the connection to a MySQL Server.
?>
<!doctype html>
<html>
<head>
<script language="JavaScript" type="text/javascript"><!--
	setTimeout("window.history.go(-1)",<?php echo $resettime_slow?>);
--></script>
<meta charset="utf-8">
<title>Create Newsletter DB</title>
</head>
<body>
<?php
	# Delete Table if already exist
	$result = mysqli_query($db_connect,"DROP TABLE IF EXISTS datatable;");
	if ($result == 1){
		echo "existiernede Table: datatable wuder gel&ouml;scht!<br />";
	}
	else{
		echo "existiernede Table: datatable konnte nicht gel&ouml;scht werden!<br />";
	}
	# Create Basic Table
	$result = mysqli_query($db_connect,"CREATE TABLE IF NOT EXISTS datatable (id int(11) NOT NULL, todo tinyint(1) NOT NULL, daynr int(11) NOT NULL) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
	if ($result == 1){
		echo "Erstelle basis Table: datatable !<br /><br />";
	}
	else{
		echo "Erstellen der basistable 'datatable' fehlgeschlagen!<br /><br />";
	}
	# Insert Dummy Data to Basic Table
	for ($i = 1; $i <= $num_newsletters; $i++) {
			$result = mysqli_query($db_connect,"INSERT INTO datatable (id, todo, daynr) VALUES (".$i.", 1, ".$i.");");
		if ($result == 1){
			echo "Anlegen von Daten f&uuml;r Tag ".sprintf('%02d',$i)." war erfolgreich <br />";
		}
		else{
			echo "Anlegen von Daten f&uuml;r Tag ".sprintf('%02d',$i)." ist fehlgelschlagen <br />";
		}
	}
	echo "<br />";
	
	# Create Project specific Columns and insert Dummy Data
	foreach ($htmlStrings as $colum){
		$result = mysqli_query($db_connect,"alter table datatable add ".$colum." VARCHAR(255)");
		if ($result == 1){
			echo "Table datatable um die Spalte ".$colum." erweitert <br />";
		}
		else{
			echo "Table datatable nicht um die Spalte ".$colum." erweitert <br />";
		}
		$result = mysqli_query($db_connect,"update datatable set ".$colum." ='".$colum."';");
		if ($result == 1){
			echo "Anlegen von Dymmy Daten f&uuml;r ".$colum." war erfolgreich <br /><br />";
		}
		else{
			echo "Anlegen von Dymmy Daten f&uuml;r ".$colum."  ist fehlgelschlagen <br /><br />";
		}
	}
	foreach ($htmlTextBlocks as $colum){
		$result = mysqli_query($db_connect,"alter table datatable add ".$colum['name']." TEXT");
		if ($result == 1){
			echo "Table datatable um die Spalte ".$colum['name']." erweitert <br />";
		}
		else{
			echo "Table datatable nicht um die Spalte ".$colum['name']." erweitert <br />";
		}
		$result = mysqli_query($db_connect,"update datatable set ".$colum['name']." ='".$colum['name']."';");
		if ($result == 1){
			echo "Anlegen von Dymmy Daten f&uuml;r ".$colum['name']." war erfolgreich <br /><br />";
		}
		else{
			echo "Anlegen von Dymmy Daten f&uuml;r ".$colum['name']."  ist fehlgelschlagen <br /><br />";
		}
	}
	
	mysqli_close($db_connect);
?>
</body>
</html>
