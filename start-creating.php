<?php
	include_once 'config.php';					// Import config.php File
	include_once 'helper/dbconnect.php';		// Returns $db_connect which represents the connection to a MySQL Server.
?>
<!doctype html>
<html>
<head>
<script language="JavaScript" type="text/javascript"><!--
	setTimeout("window.history.go(-1)",<?php echo $resettime_fast?>);
--></script>
<meta charset="utf-8">
<title>Create Newsletter</title>
</head>
<body>
<?php
	include 'helper/reset-todo.php';
	echo "<br />";
	for ($i = 1; $i <= $num_newsletters; $i++) {
		$newsletter = file_get_contents($urlhost.'create-page.php');
    	$filename = $outputdir.'/newsletter_nr'. sprintf( '%02d', $i ) . '.html';
		if (!file_exists($outputdir)) {
		    mkdir($outputdir, 0777, true);
		}
		file_put_contents($filename, $newsletter);
		echo 'Datei  '.$outputdir.'/newsletter_nr'. sprintf( '%02d', $i ) . '.html erstellt...<br />';
	}
	include 'helper/reset-todo.php';
	mysqli_close($db_connect);
?>	
</body>
</html>
