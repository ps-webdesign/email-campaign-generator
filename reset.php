<?php
	include_once 'config.php';					// Import config.php File
	include_once 'helper/dbconnect.php';		// Returns $db_connect which represents the connection to a MySQL Server.
?>
<!doctype html>
<html>
<head>
<script language="JavaScript" type="text/javascript"><!--
	setTimeout("window.history.go(-1)",<?php echo $resettime_fast?>);
--></script>
<meta charset="utf-8">
<title>Reset todo in DB and go back</title>
</head>
<body>
<?php
	include 'helper/reset-todo.php';
	mysqli_close($db_connect);
?>
</body>
</html>
