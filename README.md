# Email Campaign Generator v0.2
- - - -
### Changelog: v0.2
* Now supporting ASCII UTF-8 and ISO-8859-1 Characters

### Initial Release
* Configuration via the configuration.php file.
* Creating a DB
* Import HTML Snippets
* Single Page output
* Export all to File System with one click
